# The Rust Programming Language

## Chapter 2 A Simple Program

### Common `rust` phrases

- `use std::io`
   To state that you need the dependency `io` in the `std`
   Rust provides (very) basic utilities defined in [prelude](https://doc.rust-lang.org/std/prelude/index.html) where functions need not be `use` before usage,
   but basically any pratical work would need `use`.

### Const Everygthing

#### Variable

By default, `rust` variables are *immutable*.
That means if you want a mutable variable, declare them w/ `let mut` instead of plain `let`.

#### Reference

Like `C++`, you could access reference of variable, instead of making a whole copy.
To access the reference of some entity, just use `&` before its name.
Notice that `rust` also default all reference to const,
which means you need `&mut` if you want to modify the actual object.

### Error Handling via `expect()`

`Result` is a generic type in `std` of the `rust` programming language;
most submodules would define a different version, such as `io::Result` for the `std::io` module.
It's of type `enum`, meant to carry the execution status of the function.
It have several methods defined, one of which is `expect`.

`expect` of the `Result` type is supposed to be an error handling feature,
doing different things according the execution status it's meant to carry out:

- `Ok`
  the `expect` method would just return the value untouched,
  effectively doing nothing.
- `Err`
  for `io::Result`, it would abort the program, printing the string you give it.
  e.g.
  ``` rust
  io::stdin() .read_line(&mut myStr) .expect("Failed to read line");
  ```
  This would print `"Failed to read line"` when the something went wrong.

Notice that we're supposed to do actual handling everytime in the `rust` world -
`rustc` would generate warnings if you don't do so, -
instead of just aborting with some messages like the example above.
This would be addressed in later chapters.

### Crate

__[Crate.io](https://crates.io/)__
The official repository for `rust` libraries.

You need to specify all the *crates* you need in your project top folder in the `Cargo.toml` file.
You could specify the version you want, e.g. `rand 0.5.4`, under the tag `[dependencies]`.
By default, this imlicitly means `rand ^0.5.4`, which implies `cargo` would actually search
for the *latest ABI compatible* version of the package `rand` if you told it to.

If you added some new dependencies in the `Cargo.toml` file,
`cargo` would check for the new libraries you added,
retriving those (according your mirror settings) from the online repository, compiling them for later use.

When you `cargo build` the project, `cargo` would also update the `Cargo.lock` file,
such that it would contain the specific version of crate you built successfully with.
This ensures reproducibility across machines: anyone cloning your project would get
the exact version of library as the one you wrote the code with, minimizing the probability
that some regression happened during version iterations, or the ABI compatibility simply broke.

Let's say you want to upgrade the crates; it's when `cargo update` comes to rescue.
It would update all the crates to the latest minor version:
to be specific, continuing the `rand 0.5.4` example, it would retrieve any `rand` crate
with version number `0.5.X` where $X$ is larger than 4 but version `0.6.0` or `0.7.1`.
To update to another minor version, you need to change the `Cargo.toml` file.
The `Cargo.lock` file would be updated once the build process successed.

You could view all the traits and/or functions defined within a crate online,
or *you could also generate __offline__ documentation with `cargo doc`* - neat!
The output would by default in HTML format, under `./target/doc` directory.

### Trait

### Shadowing

We could reuse a variable name such that it could have a different type and semantic
meaning in the later context.

``` rust
let mut guess = String::new();
// some code filling `guess` with value
let guess: i32 = guess.trim().parse().expect("Please type a number!");
// `guess` is now signed 32-bit integer
```

Note that by default, `rust` standard library `std::io::read_line(&mut str)` would also
pend the `newline` character at the end;
`trim()` method discards any `whitespace` character in the front or back.

`parse()` method transforms the string into a number, according to the type of the
variable it shall overwrite.
Since it could fail on some non-integer input, it returns a `Return` type,
and we shall put corresponding `expect()` method such that it don't *panick*.
For example, `'*'`, empty string, or overflow/underflow would cause the
`parse()` method to panick later on.

Shadowing could also modify mutability.

### Error Handling via `match`

``` rust
let guess: u32 = match guess.trim().parse() {
   Ok(num) => num,
   Err(_) => continue,
};
```

`'_'` serves as a catchall here: the enum `Result` carries information, such as the return
value of the function who returned it, or the panick reasons. We just tell it to ignore them
and `continue` the loop, as specified in the *arm*.
