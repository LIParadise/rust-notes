#[allow(non_snake_case)]

use rand::Rng;

#[allow(non_camel_case_types)]
#[allow(dead_code)]
#[derive(Debug)]
struct some_user {
    name: String,
    id: u32,
    active: bool,
}

impl some_user {
    fn print_id(&self) {
        println!( "{}", self.id );
    }
}

fn main() {

    let s1 = some_user {
        name: String::from( "what" ),
        id:   rand::thread_rng().gen_range(0, 42),
        active: true,
    };

    let s2 = some_user {
        name: String::from( "dio sama" ),
        id:   rand::thread_rng().gen_range(0, 100),
        active: true,
    };

    let s3 = some_user {
        name: String::from( "compile" ),
        ..s2
    };

    let s4 = some_user {
        name: s1.name,
        ..s3
    };

    println!( "{:?}", s2 );
    s1.print_id();
}
