use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
   let secret_number: i32 = rand::thread_rng().gen_range(1, 101);

   println!("Guess the number!");

   println!("The secret number is: {}", secret_number);

   println!("Please input your guess.");

   loop{
      let mut guess = String::new();
      let mut tmp: i32 = 42;

      io::stdin()
         .read_line(&mut guess)
         .expect("Failed to read line");

      let guess: i32 = match guess.trim().parse() {
         Ok(tmp) => tmp,
         Err(num) => continue,
      };

      println!("You guessed: {}, \'tmp\' is {}", guess, tmp);

      match guess.cmp(&secret_number) {
         Ordering::Less => println!("Too small!"),
         Ordering::Greater => println!("Too big!"),
         Ordering::Equal => {
            println!("You win!");
            break;
         }
      }
   }
}

