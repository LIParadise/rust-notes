use std::io;
use std::{thread, time};


fn main() {
    let ten_ms = time::Duration::from_millis(10);
    println!("Please input your guess.");
    let mut guess = String::new();
    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    let mut guess: i32 = match guess.trim().parse() {
        Ok(tmp) => tmp,
        Err(e) => panic!("Invalid Number Format, Your Input is {}", e)
    };

    let mut counter: i32 = 0;
    let mut guess2: i32 = 0;

    counter = loop{
        guess = plus_1(guess);
        guess2 = guess + counter;
        counter = counter+1;
        if guess2 % 89 == guess % 89{
            break counter
        }
    };

    println!( "counter is {}", counter );
    println!( "guess is {}", guess );
    println!( "guess2 is {}", guess2 );

}

fn plus_1(i: i32) -> i32{
    i+1
}

fn minus_1(i: i32) -> i32{
    i-1
}
