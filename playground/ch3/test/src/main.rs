use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 11);
    let mut y: i32       = -1;
    let mut counter: i32 = 0;

    let counter_bak = loop {
        println!("Please input your guess.");

        let mut guess = String::new();
        counter = counter+1;

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You guessed: {}", guess);

        y = match guess.cmp(&secret_number) {
            Ordering::Less => {
                println!("Too small!");
                0
            }
            Ordering::Greater => {
                println!("Too big!");
                1
            }
            Ordering::Equal => {
                println!("You win!");
                break counter
            }
        };
    };

    println! ("y is {}", y);
    println! ("counter_bak is {}", counter_bak);
}
