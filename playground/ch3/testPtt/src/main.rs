use std::io;

fn main() {
    println!("Please input your guess.");
    let mut guess = String::new();
    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    let guess: i32 = match guess.trim().parse() {
        Ok(tmp) => tmp,
        Err(e) => panic!("Invalid Number Format, Your Input is {}", e)
    };

    if guess == 723 {
        println!("\"Wargame723\" detected!!");
    }else if guess == 157 {
        println!("\"Tsukiyomi157\" detected!!");
    }else {
        println!("Continue");
    }
}
