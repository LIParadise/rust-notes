use std::cmp::Ordering;
use std::io;

fn main() {
    let mut userInput: String = String::new();
    let zero: i32 = 0;

    io::stdin()
        .read_line( &mut userInput )
        .expect( "io failure" );

    let userInput: i32 = userInput
        .trim()
        .parse()
        .expect( "not an 32-bit integer" );

    let comment = match userInput.cmp(&zero) {
        Ordering::Less => "Negative",
        Ordering::Greater => "Positive",
        Ordering::Equal => "Zero"
    };

    println! ( "Your input is \"{}\"", &comment );
}
