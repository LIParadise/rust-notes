use std::io;

fn main() {
    println!("Guess the number!");

    let mut counter: i32 = 0;

    let what = loop {
        if counter == 4 {
            break 42069
        };
        counter += 1;

        let mut guess = String::new();
        io::stdin().read_line(&mut guess).expect("Failed to read line");
        let guess: u32 = guess.trim().parse().expect("invalid number format");

        if guess % 4 == 0 {
            break 424
        }else if guess % 5 == 0 {
            break 425
        }
    };

    println! ("counter     is {}", counter    );
    println! ("counter_bak is {}", what       );
}
