#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

fn main() {
    struct myClock{
        sec: usize,
        min: usize,
        hr: usize,
    }

    impl myClock {
        fn tick(&mut self){
            self.sec += 1;
            if self.sec >= 60 {
                self.sec = 0;
                self.tock();
            }
        }
    }

    impl myClock {
        fn tock(&mut self){
            self.min += 1;
            if self.min >= 60 {
                self.min = 0;
                self.fuck();
            }
        }
    }

    impl myClock {
        fn fuck(&mut self){
            self.hr += 1;
            if self.hr >= 24 {
                self.hr = 0;
            }
        }
    }

    impl myClock {
        fn tellTime(&self){
            let h = &self.hr.to_string()[..];
            let m = &self.min.to_string()[..];
            let s = &self.sec.to_string()[..];
            // let h: &str = if self.hr  == 0 {"00"} else {&self.hr.to_string()[..]};
            // let m: &str = if self.min == 0 {"00"} else {&self.min.to_string()[..]};
            // let s: &str = if self.sec == 0 {"00"} else {&self.sec.to_string()[..]};
            // Return values are unnamed temporaries;
            // It's only when *immediately* bound to some other named variable, say `v`,
            // the lifetime follows `v`, whether `v` being `&` or not
            // or it's just discarded at the end of the enclosing statement,
            // rendering any usage after it illegal.
            // 
            // Notice if `v` is parameter of some other function, or `v` is to vanish anyways somehow,
            // care must be taken, since when `v` went out of scope, there's NO one holding the originally produced value,
            // rendering anything using it again illegal.
            //
            // `rust` prevents this via set of compiler rools.
            // check `rustc --explain E0716`
            let h: &str = if self.hr  == 0 { "00" } else {h};
            let m: &str = if self.min == 0 { "00" } else {m};
            let s: &str = if self.sec == 0 { "00" } else {s};
            println!("{}:{}:{}", h, m, s);
        }
    }

    let mut c = myClock{
        hr: 11,
        min: 19,
        sec: 52,
    };

    c.tick();
    c.tick();
    c.tick();
    c.tick();
    c.tick();
    c.tick();
    c.tick();
    c.tick();
    c.tock();
    c.tock();
    c.tock();
    c.tock();
    c.tock();
    c.tock();
    c.tock();
    c.tock();
    c.tellTime();

}
