# The Rust Programming Language

## Chapter 1 Getting Started

### A Little `rust` Basics

- function names that end with `'!'` means it's in fact a macro, not a function
  - e.g.: `println!` is a rust macro.

### `cargo`, the Build And Dependency Control System

#### New Repository

`cargo new <project>` creates a new project folder,
inside which is

- `Cargo.toml`, specifying build options and dependencies,
- `.gitignore`: yes, `cargo` automatically initiate version control, defaulting to `git`
  - `cargo` would not do so if it's already in `git`; `cargo new --cvs=git` to override.
- `src`: conventionally rust likes to put all the code in `src`

#### Grammar Check

`cargo check`

Just run linting.

#### Debug Release

`cargo build`

By default, `cargo` would just build debug version without optimization for quick testing.
The executable would be in `./target/debug`

#### Production Release

`cargo build --release`
The executable would be in `./target/release`

#### Run the Binary

`cargo run`
This would also re-compile if anything have changed before last build.
