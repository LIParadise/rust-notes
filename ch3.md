# The Rust Programming Language

## Chapter 3 Basic Rust Concepts

### Chapter 3.1 Variables and Mutability

#### Mutability, `mut`

By default, `rust` variables are not allowed to change, improving code clarity.
This is highly related to the programming style known as *functional programming*.
However you could still modify a variable with the `mut` keyword,
for instance you may not want to copy a huge data structure on the fly,
modifying only one small element of it in-place instead.

#### Contants, `const`

Must be declared with its type explicitly defined.

In `rust`, constants could only be initiated with *constant expressions*,
for example you could not assign them with return values of functions.

#### Shadowing with `let`

``` rust
let spaces = "     ";
let mut spaces = spaces.len();
```

You could change mutability and/or type with the `let` expression,
rendering reusing variable names possible.

### Chapter 3.2 Data Types

#### Overflow

Overflow and/or underflow could happen.
By default, when compling with debug mode, `rust` includes checks s.t. when
overflow occurs the program would `panic`.
But when compiling with `--release` mode, `rust` don't do such checks; instead
it would just perform ordinary two's complement calculation, or *wrapping*, s.t.
for example if `u8` then `257` is again `1`.
You could however force `rust` to always do wrapping with the `Wrapping` type.

#### Common Types

- Integer
  `rust` default to `i32`,
  whereas when indexing an array `rust` allow only `usize` or `isize`.
  | unsigned | signed | meaning |
  |---|---|---|
  | u8 | i8 | 8 bit integer |
  | u16 | i16 | 16 bit integer |
  | u32 | i32 | 32 bit integer |
  | u64 | i64 | 64 bit integer |
  | u128 | i128 | 128 bit integer |
  | usize | isize | defined by machine architecture |
- Boolean
  `bool`, always `1` byte.
- Characters
  In `rust` they are `4` bytes, representing a Unicode value.
  The range is `U+0000` to `U+D7FF` and `U+E000` to `U+10FFFF` inclusive.
  Use `''` to declare them, for example `let heart_eyed_cat = '😻';`

#### Compound Types

- Tuple
  ``` rust
  let tup: (i32, f64, u8) = (500, 6.4, 1);
  let (x, y, z) = tup;
  let w=tup.2
  println!("The value of y is: {}, w is: {}", y, w);
  ```
  `Tuple` have fixed length an could be indexed, starting at 0.
  You could specify the exact types the element of a tuple shall be, by specifying `()`.
- Array
  ``` rust
  let a: [i32; 5] = [1, 2, 3, 4, 5]; // specify type and length of array at once.
  let b = [1, 2, 3, 4, 5];
  let c = [3; 5];                    // initialize array with some fixed value
  ```
- fixed length
  `rust` `array` is fixed in length -- you may want to checkout the `vector` provided by
  `rust` standard library.
- out of bound protection
  Accessing out of bound would `panic` during runtime.
  Notice though it's fixed in length, it's not compile time determined.

### Chapter 3.3 Functions

`rust` don't care if you defined a function - `fn` declares one if you dunno - before or after its name is used: it's okay if it's defined somewhere else. This is unlike `C` or `C++`, where you have to at least declare the symbol with how it shall be called before you use it.

`rust` function bodies are made up of a series of statements, optionally ending in an expression.

> `rust` is an *expression-based* language

#### Expression

A piece of code that evaluates to some value.

- `42`
  Being the answer to everything, it's also an expression in `rust`.
- `x+42`
  `42` is so powerful that any variable `+42` *without* the ending `;` is also an expression.
- ``` rust
  let x = {
  let my_num = loop {
     my_string.clear();
     stdin().read_line(&mut my_string)
        .expect(“Did not enter a correct string”);
     match my_string.trim().parse::<f64>() {
        Ok(_s) => break _s,
        Err(_err) => println!(“Try again. Enter a number.”)
     }
  };
  ```
  `{}`, curly brackets, which introduces new scopes, is an expresion
- calling a function
- calling a macro

If you add a `;` at the end of an expression, it's no longer expression; it's now a statement, which leads us to the next chapter.

#### Statement

A piece of code that operates on something but can't be evaluated to an value.

- ``` rust
  let x: i32 = 2147483647;
  let y: i32 = bar_42069(x);
  ```
  Or basically everything ending with `;`.
  BTW this code segment would `panic` during runtime if you didn't turn off the overflow check.
- function definitions

#### Return Values

`rust` specify return type like the following:

``` rust
fn giveMeFive() -> i32 {
   5
}
```

You could take advantage of the fact that a pair of `{}` with its ending line being an expression is also an expression to write function return values.  Also, you could just use the good old `return` keyword, in case you want early return or just want to clarify.

``` rust
  fn bar_42069(x: i32) -> i32{
     return x+42069;
  }
```

You could also use the `return` keyword. Notice here the `;` at the end of the `return` line could be dismissed.

To sum up, you need *tail expression* or `return` keyword to return values, and it needs to be of the specified type.

### Chapter 3.5 Control Flows

In `rust`, all conditions for `if` *must* be of `bool` type.

Notice that since in `rust` a block of code could end with an `expression` where that block would evaluate to, the control flow design could give several different possible expressions. One thing to note is that in this case the types of all the possible final expression candidates shall be the same: `if` code block and `else` code block should have the same type. For instance, this code would not compile:

(Recall that blocks of code would evaluate to its final expression)

``` rust
fn main() {
   let condition = true;
   let number = if condition { 5 } else { "six" };
   println!("The value of number is: {}", number);
}
```

- `if`, `else if`, `else`
- `loop`
- `while`
- `for`
  - use with `collection` and `Range` type; the latter provided by `rust` standard library
    ``` rust
    let a = [10, 20, 30, 40, 50];
    for element in a.iter() {
      println!("the value is: {}", element);
    }
    for idx in (42..69).rev() {
      println!("Dio Sama!!! {}, idx);
    }
    for element in a.iter().rev() {
      println!("the value is: {}", element);
    }
    ```
- `break`
  - notice when tailing `break`, either adding `;` at the end or not helps evaluate the `{}` block expression
    ``` rust
    let mut times: i32 = 42;
    let mut num: i32 = 9;
    let num: i32 = loop {
      times -= 1;
      num += times;
      if times == 0 {
        break num*3
        // this above line could optionally add ";"
      }
    };
    ```
