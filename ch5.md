# Chapter 5. `struct`

> Structs and enums (discussed in Chapter 6) are the building blocks for creating new types in your program’s domain to take full advantage of Rust’s compile time type checking.

## Chapter 5.1. Define and Instantiation

### Define A Struct

``` rust
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}
```

### Instantiate A Struct

As long as your scope have variables of exactly the same name, you could elide some code.

``` rust
fn main() {
    user1 = build_user( String::from("lwn.net"), String::from("Linus Torvalds") );
}
fn build_user(email: String, username: String) -> User {
    User {
        email,
        username, // notice we don't need explicity `:` here.
        active: true,
        sign_in_count: 1,
    }
}
```

### Instantiate A Struct with Other Struct

Notice `struct` could be *partly* `borrow`-ed, and this affects the `struct` itself: you can't `println!("{:?}", user1);` after some of its member got `borrow`-ed, neither could you call *any* method on `user1`. You could still legally use other not `borrow`-ed members of `user1`, though.
You could also elide some code with the `..` syntax; nothing but a syntax sugar, so the `borrow` rules apply.

``` rust
let user2 = User {
	email: String::from("example.com"),
   username: user1.username,
   ..user1
};
println!( "\"user1\" have \"email\" \"{}\"", user1.email );
```

### Tuple Structs

``` rust
	struct Color(i32, i32, i32);
	struct Point(i32, i32, i32);

	let black = Color(0, 0, 0);
	let origin = Point(0, 0, 0);
```

Notice these are different type even though their fields have same types.

### Unit-Like Structs without Any Fields

Behave similarly to `()`, the unit type.
A common usage is around `trait`s.

### Ownership of Struct Data

It's possible for `struct` to store references, but this require `lifetime` syntax, which help ensure the validity of references whenever the `struct` is still valid.
