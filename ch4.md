# Chapter 4. `ownership`

`ownership` is an important feature enabling `rust` with memory safety guarantees without garbage collector

In this chapter, we'll walk through several aspects of `ownership`:

- *borrowing*
- *slices*
- `rust` data layout in memory

## Chapter 4.1. What is `ownership`?

`rust` introduced the term `ownership` to help deal with *memory management*.
There are $3$ basic rules:

1. Each value in Rust has a variable that’s called its *owner*.
2. There can only be *one* owner at a time.
3. When the owner goes out of scope, the value will be *dropped*.

A variable could be *in scope* or *out of scope*...

### Scope and `trait`s like `drop`

When the owner of some *heap* memory goes out of scope, or to be specific, types with `trait` `drop`, the memory is returned to system via calling `drop()`, kinda like the __RAII__ philosophy of `C++`.

Consider the following code:

``` rust
// userInput: mut String
// userInputDigit: uint
let myString0 = userInput.clone();
let myDigit   = userInputDigit;
let myString1 = userInput;
```

To improve performance and to ensure memory management, types of `trait` `drop` when assigned to another variable, it invalidates itself, transferring control of its resource to the new variable.
For example `myString1` now internally have a pointer pointing to which previously the internal pointer of `userInput` points to on heap memory.
That is, data types with `trait` `drop` by default are *moved*.

In contrast, types that have `trait` `copy` means that the old variable would maintain valid after the assignment. Types on stack memory are allowed to have `copy` `trait`, and most built-in scalar types do.

Usually when you want a deep copy -- `copy assignment constructor` in `C++` terms, if you wish -- or you want to remain the validity of the old variable for types with `drop`, you could call its `clone()` function.

But as demonstrated below, this policy could be a lot of hassle, especially if we want to call functions:

``` rust
use std::io;
fn main(){
   let mut userInput = String::new();
   std::io::stdin()
      .read_line( &mut userInput )
      .expect( "invalid stdin" );
   let ret = addDioSamaAndGetOriginalLength(userInput);
   println! ( "input: \"{}\", length \"{}\"", ret.0, ret.1 );
}
fn addDioSamaAndGetOriginalLength(mut s: String) -> (String,usize) {
   let l = s.len();
   s.push_str( "Dio Sama!!!" );
   (s, l)
}
```

Since `userInput`, of type `String` and `trait` `drop`, is invalidated after `addDioSamaAndGetOriginalLength`, we need to somehow get it back; this means lots of things to `return`.
This leads us to __references__.

## Chapter 4.2. Reference

``` rust
#![allow(non_snake_case)]
fn main() {
   let mut myString: String = String::from("Dio Sama!!!");
   orig(&myString);
   plus1(&mut myString);
   let ref0 = & myString;
   println! ( "last seen: \"{}\"; \"{}\"", myString, ref0 );
}
fn orig(s: &String) -> (){
   println! ( "\"myString\" containes {}", s );
}
fn plus1(s: &mut String) -> (){
   s.push_str( "Polnareff" );
   println! ( "\"myString\" containes {}", s );
}
```

You could `borrow` (yep, this is how Rustaceans call it) a variable with references. You could borrow it as `&mut` such that you could modify it. The original variable would retain ownership.

Note that references have their scopes __*ends where it's lastly used*__.

Note also that to ensure no data racing, you could either have 1 `&mut` or several `&` of some variable: `&mut` and `&` don't coexist in the same scope.

Moreover, `&mut` only binds to `mut`, and if you do, you *can't read nor write* to the original variable; all accesses are limited to via usage of the `&mut` to prevent any possible data race.
On the other hand, `&` don't mind if you are `mut`, but if there's `&` haven't gone out of scope, *any* access is read-only; you could still read the original variable, though.

### Examples

1. When `borrow`-ed with `&`, the original data is read-only; when `&mut`, the original data isn't accessible
   ``` rust
   fn main() {
      let mut userInput = String::new();
      io::stdin()
         .read_line( &mut userInput )
         .expect( "Invalid input string" );
      let mut userInput: i32 =
         userInput.trim().parse().expect ( "Invalid string format" );
      let ref0 = &mut userInput;
      userInput += 1; // compilation ERROR here!! E0503
      println!("Dio Sama!!! {}", ref0 );
      println!("Dio Sama!!! {}", userInput );
   }
   ```
2. You could have either one `mut` reference, or many genuine references at the same scope. That is, you can't simutaneously have `&` and `&mut` of the same piece of data in the same scope.
   This below is okay: `&mut ref1` is last used before `& ref0` is created; `ref1` had gone out of scope when `ref0` enters scope.
   ``` rust
   fn main() {
      let mut userInput = String::new();
      io::stdin()
         .read_line( &mut userInput )
         .expect( "Invalid input string" );
      let mut userInput: i32 =
         userInput.trim().parse().expect ( "Invalid string format" );
      let ref1 = &mut userInput;
      *ref1 += 1;
      let ref0 = & userInput;
      println!("Dio Sama!!! {}", ref0 );
      println!("Dio Sama!!! {}", userInput );
   }
   ```
   This below don't compile, since there's 1 `&mut` and 1 `&` in the scope
   ``` rust
   fn main() {
      let mut userInput = String::new();
      io::stdin()
         .read_line( &mut userInput )
         .expect( "Invalid input string" );
      let mut userInput: i32 =
         userInput.trim().parse().expect ( "Invalid string format" );
      let ref0 = &mut userInput;
      let ref1 = & userInput;
      println!("Dio Sama!!! {}", ref1 );
      *ref0 += 1;
      println!("Dio Sama!!! {}", ref0 );
      println!("Dio Sama!!! {}", userInput );
   }
   ```

### More Subtleties

- Scope of an reference *__ends__ where it's last used*
  ``` rust
  let mut s = String::from("hello");
  let r1 = &s;
  let r2 = &s;
  println!("{} and {}", r1, r2);
  // r1 and r2 are no longer used after this point
  let r3 = &mut s; // no problem!!!
  println!("{}", r3);
  ```

## Chapter 4.3. The `slice` Type

We'll focus on *string slices* here.

``` rust
#![allow(non_snake_case)]
fn main(){
   let mut userInput = String::new();
   std::io::stdin()
      .read_line( &mut userInput )
      .expect( "invalid stdin" );
   let DioSama = "Dio Sama!!!";
   let mut output = String::new();
   output.push_str( first_word(&userInput) );
   output.push_str( DioSama );
   println!( "{}", output );
}
fn first_word(s: &String) -> &str {
   let bytes = s.as_bytes();
   for (i, &item) in bytes.iter().enumerate() {
      if item == b' ' {
         return &s[0..i];
      }
   }
   &s[..]
}
```

String slices are of type `&str`.
Since string slices are references (*fat pointers* here) to `String`, which kinda behaves like `&String`, we could prevent some trivial errors such as destroying the `mut String` with function like `userInput.clear()`, which is induces *mutable borrow*.

There are some subtleties here not addressed, such as dealing with UTF-8 means we can't quite use `String` or `&str` in bytes, and that there's more kinds of slices: string slices is just one of them.

``` rust
let a = [1, 2, 3, 4, 5];
let slice = &a[1..3];
```

