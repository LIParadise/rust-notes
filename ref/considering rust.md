# Considering Rust

[youtube](https://www.youtube.com/watch?v=DnT-LUQgc7s)

## Comparison to Other Common Languages

### Python

- Much Faster
- complied language
    - no bytecode, just machine code
- algebraic data types
- pattern matching

### Java

- Faster
- Zero-cost Abstractions
- No data-races guaranteed at compile time
- Pattern Matching
- unified build system
- dependency management

### C/C++

- No segfault, no buffer overflow, no null pointer
- No data races
- unified build system
- dependency management

### Go

- No GC
- No null pointer
- nicer error handling
- safe concurrency
  - Go is easy for concurrency, but it's also easy to shoot yourself in your foot with
- Zero-cost Abstractions
- dependency management

## Primary Features

- Modern Language
  - you don't feel low-level
   - algebraic types and pattern
   - modern tooling
  - #[test]
    - unit test
    - integration tests
  - ///
    - automatic documentation generation
    - embedded code blocks $\rightarrow$ integration tests
    - documentation as tests
- Safety by Construction
  - pointers are checked at compile time
    - single owner
      - C++ RAII
    - no dangling pointers/use-after-free
      - `borrow checker`
  - thread-safety from types
    - > rust types know whether it's safe for them to cross thread boundaries
      > they know whether it's safe for a value to be given away to another thread
      > they also know whether it's safe for two threads to access to the value at the same time
    - `rayon`
  - no hidden states
    - you don't see no null pointers in rust
      - every reference you see is guaranteed not to be null
        - optimizations based on this guarantee is made possible!
      - `?`
- Low-Level Control
  - no gc or runtime
    - can run without an OS
    - free ffi
      - java jni
      - python c++ binding
      - go cgo
  - control allocation and dispatch
    - `#![global_allocator]`
      - jemalloc
      - (google) tcmalloc
      - (microsoft) mimalloc
    - `&dyn`
  - can write + wrap low-level code
    - `unsafe`
    - > The safe code will always be memory-safe
- Compatibility
  - zero-overhead ffi
    - > `ld` is gonna linke these two together, and they'll be co-optimized with like LTO
    - `#[no_mangle]`
    - bindgen
    - cbindgen
    - makes some rewrite easy
      - e.g. you could module by module rewrite a C++ program into rust
  - WebAssembly support
    - rust program as npm module
  - work with traditional tools
    - perf
    - gdb/lldb
    - valgrind
    - llvm sanitizers
    - dwarf
- tooling
  - dependency management
    - semantic versioning
    - rust build files for other language dependencies
    - different build could have different dependencies
      - you may want no `quickcheck` in your optimized build
  - standard tools included
  - excellent support for macros
    - > rust has really good support for writing rust programs that manipulate rust programs
      - metaprogramming
    - > they're fully fledged rust programs that have well-defined syntax for transforming rust trees
    - hygienic
      - any identifier you define inside the macro *cannot* contaminate the space outside the macro, and any identifier you have outside the macro, you cannot modify inside the macro.
    - `procedural macro`
      - `serde`
        - serialization and deserialization automatically for any type for any format
        - `#[derive(Serialize, Deserialize)]
        - generic
          - json serializer
          - csv serializer
          - binary encoding serializer
          - gRPC serializer
- Asynchronous Code
  - Language support for writing asynchronous code
  - choose your own runtime!
  - still evolving

## Drawbacks of rust

- Learning curve
  - the borrow checker
  - no object-oriented programming
    - similar concepts exist
    - it's for reasons...
      - zero cost abstractions
      - thread safety checked at compile time
- ecosystem
  - young, and few maintainers
  - small
    - but growing, quickly
- no runtime
  - no runtime reflection
  - no runtime-aided debugging
- compile
  - slow
    - improving, though
    - have incremental compiling support
  - no pre-built libraries
- vendor support
  - huge C++ libraries are a pain
  - tooling that only supports C++
- Windows
  - full compiler/std support
  - limited library support

## Long Term Viability

